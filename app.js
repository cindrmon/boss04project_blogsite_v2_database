// linter silencer, for some reason
//jshint esversion:6

// module imports ---------------------------------------

    const bodyParser = require("body-parser");
    const ejs = require("ejs");
    const express = require("express");
    const mongoose = require('mongoose');
    const _ = require("lodash");
    require('dotenv-flow').config();

// ------------------------------------------------------

// express to app init ----------------------------------

    const app = express();

    app.set('view engine', 'ejs');

    app.use(bodyParser.urlencoded({extended: true}));
    app.use(express.static("public"));

// ------------------------------------------------------

// custom global variables ------------------------------

    const Port = process.env.PORT || 3000; // custom ports

    const Config = {
        root_password: process.env.ROOT_PASSWD
    }; // separate password to a file

    const ContentSize = 50; // max characters in the home page

    // default content
    const homeStartingContent = "Lacus vel facilisis volutpat est velit egestas dui id ornare. Semper auctor neque vitae tempus quam. Sit amet cursus sit amet dictum sit amet justo. Viverra tellus in hac habitasse. Imperdiet proin fermentum leo vel orci porta. Donec ultrices tincidunt arcu non sodales neque sodales ut. Mattis molestie a iaculis at erat pellentesque adipiscing. Magnis dis parturient montes nascetur ridiculus mus mauris vitae ultricies. Adipiscing elit ut aliquam purus sit amet luctus venenatis lectus. Ultrices vitae auctor eu augue ut lectus arcu bibendum at. Odio euismod lacinia at quis risus sed vulputate odio ut. Cursus mattis molestie a iaculis at erat pellentesque adipiscing.";
    const aboutContent = "Hac habitasse platea dictumst vestibulum rhoncus est pellentesque. Dictumst vestibulum rhoncus est pellentesque elit ullamcorper. Non diam phasellus vestibulum lorem sed. Platea dictumst quisque sagittis purus sit. Egestas sed sed risus pretium quam vulputate dignissim suspendisse. Mauris in aliquam sem fringilla. Semper risus in hendrerit gravida rutrum quisque non tellus orci. Amet massa vitae tortor condimentum lacinia quis vel eros. Enim ut tellus elementum sagittis vitae. Mauris ultrices eros in cursus turpis massa tincidunt dui.";
    const contactContent = "Scelerisque eleifend donec pretium vulputate sapien. Rhoncus urna neque viverra justo nec ultrices. Arcu dui vivamus arcu felis bibendum. Consectetur adipiscing elit duis tristique. Risus viverra adipiscing at in tellus integer feugiat. Sapien nec sagittis aliquam malesuada bibendum arcu vitae. Consequat interdum varius sit amet mattis. Iaculis nunc sed augue lacus. Interdum posuere lorem ipsum dolor sit amet consectetur adipiscing elit. Pulvinar elementum integer enim neque. Ultrices gravida dictum fusce ut placerat orci nulla. Mauris in aliquam sem fringilla ut morbi tincidunt. Tortor posuere ac ut consequat semper viverra nam libero.";

// ------------------------------------------------------

// mongodb ----------------------------------------------

    // connect to mongodb
    mongoose.connect("mongodb+srv://root:" + Config.root_password + "@blogsitecluster.p8mct.gcp.mongodb.net/BlogsiteDB", {useNewUrlParser:true, useUnifiedTopology: true, useFindAndModify: false});

    // blog posts schema
    const POSTS_SCHEMA = new mongoose.Schema(
        {
            BlogPostTitle: String,
            BlogPostContent: String,
            BlogPostURL: String
        }
    )

    // blog posts model
    const BlogPostsMdl = mongoose.model('BlogPost', POSTS_SCHEMA);

// ------------------------------------------------------

// app.get commands -------------------------------------
    // app.get to home/root
        app.get( '/', (req,res) => {

            BlogPostsMdl.find((err,posts)=>{

                if(err)
                    console.log(err);

                else{

                    res.render('home', {
        
                      HomeContent: homeStartingContent,
                      BlogPosts: posts,
                      ContentSizeInHome: ContentSize,
                    });

                }

            });

        });

    // app.get to home/posts/:postName
        app.get('/posts/:PostName', (req,res) => {

            let PostNameInLink = _.kebabCase(req.params.PostName);

            BlogPostsMdl.findOne({BlogPostURL: PostNameInLink}, (err,foundPosts) => {

                if(err)
                    console.log(err);

                else{

                    if(!foundPosts)
                        res.redirect('/404');

                    else{

                        res.render('post', {

                            PostIndividualTitle: foundPosts.BlogPostTitle,
                            PostIndividualContent: foundPosts.BlogPostContent
      
                        }); // inserts post title and post content into a post that is assigned a link

                    }

                }

            });

        }); // assigns a specific post to a specific link that is acquired from req.params.PostName variable

    // app.get to about
        app.get( '/about', (req,res) => {
            res.render('about', {AboutContent: aboutContent});
        });

    // app.get to contact
        app.get( '/contact', (req,res) => {
            res.render('contact', {ContactContent: contactContent});
        });

    // app.get to compose (hidden)
        app.get( '/compose', (req,res) => {
            res.render('compose');
        });

    // app.get to error 404 landing page for some unknown reason
        app.get('/404', (req,res) => {
            res.render('404');
        })

        
    // app.post requests (/compose)
        app.post( '/compose', (req,res) => {
            
            const blogpostTitle = req.body.JournalTitle;
            const blogpostContent = req.body.JournalContent;
            const blogpostURL = _.kebabCase(req.body.JournalTitle);
            
            // mongoose add the item into the blogpost collections
            
            const UsrAddedBlogPost = new BlogPostsMdl(
                {
                    BlogPostTitle: blogpostTitle,
                    BlogPostContent: blogpostContent,
                    BlogPostURL: blogpostURL
                }
            )
                
            UsrAddedBlogPost.save((err) => {
                if(!err)
                    res.redirect('/');
            });
                
        });
            
    // app.post requests (/404)
        app.post('/404', (req,res) => {
            res.redirect('/');
        });

// ------------------------------------------------------

// port listener ----------------------------------------

    app.listen(Port, () => {
        console.log("Server started on port " + Port);
    });

// ------------------------------------------------------
